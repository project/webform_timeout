<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform_timeout\WebformTimeoutStorage;

/**
 * @file
 * Webform Timeout module.
 */

/**
 * Implements hook_theme().
 */
function webform_timeout_theme() {
  return [
    'webform_timeout' => [
      'variables' => [
        'content' => '',
        'expired' => '',
        'attributes' => [],
        'timeout_days' => [],
        'timeout_hours' => [],
        'timeout_minutes' => [],
        'timeout_seconds' => [],
      ],
    ],
  ];
}

/**
 * Perform alterations before a webform submission form is rendered.
 *
 * This hook is identical to hook_form_alter() but allows the
 * hook_webform_submission_form_alter() function to be stored in a dedicated
 * include file and it also allows the Webform module to implement webform alter
 * logic on another module's behalf.
 *
 * @param array $form
 *   Nested array of form elements that comprise the webform.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the form. The arguments that
 *   \Drupal::formBuilder()->getForm() was originally called with are available
 *   in the array $form_state->getBuildInfo()['args'].
 * @param string $form_id
 *   String representing the webform's id.
 *
 * @see webform.honeypot.inc
 * @see hook_form_BASE_FORM_ID_alter()
 * @see hook_form_FORM_ID_alter()
 *
 * @ingroup form_api
 */
function webform_timeout_webform_submission_form_alter(array &$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  if (isset($form['elements']['webform_timeout'])) {

    if ($form['actions']['submit']['#access'] == FALSE) {
      $form['actions']['submit']['#access'] = TRUE;
      $form['actions']['submit']['#prefix'] = '<span class="hidden">';
      $form['actions']['submit']['#suffix'] = '</span>';
    }

    $form['actions']['submit']['#submit'][] = 'webform_timeout_save_submission_timeout_submitted';
    $form['actions']['submit']['#submit'][] = 'webform_timeout_delete_timeout_submitted';
  }
}

/**
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function webform_timeout_save_submission_timeout_submitted($form, FormStateInterface &$form_state) {
  $form_object = $form_state->getFormObject();
  $webform_submission = $form_object->getEntity();

  $current_time = Drupal::time()->getCurrentTime();
  $created = $form["elements"]["webform_timeout"]['#created'];
  $distance = $current_time - $created;

  $webform_submission->setElementData('webform_timeout', $distance)->save();
}

/**
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function webform_timeout_delete_timeout_submitted($form, FormStateInterface &$form_state) {
  $user = Drupal::currentUser();
  $form_object = $form_state->getFormObject();
  $webform_submission = $form_object->getEntity();
  $webform = $webform_submission->getWebform();
  WebformTimeoutStorage::delete($webform->id(), $user->id());
}

/**
 * Alter webform elements.
 *
 * @param array $element
 *   The webform element.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the form.
 * @param array $context
 *   An associative array containing the following key-value pairs:
 *   - form: The form structure to which elements is being attached.
 *
 * @throws Exception
 * @see \Drupal\webform\WebformSubmissionForm::prepareElements()
 * @see hook_webform_element_ELEMENT_TYPE_alter()
 */
function webform_timeout_webform_element_alter(array &$element, \Drupal\Core\Form\FormStateInterface $form_state, array $context) {
  if ($element['#type'] == 'webform_timeout') {
    $form_object = $form_state->getFormObject();
    $webform_submission = $form_object->getEntity();
    $webform = $webform_submission->getWebform();

    $current_time = Drupal::time()->getCurrentTime();
    $user = Drupal::currentUser();

    if (!WebformTimeoutStorage::exists($webform->id(), $user->id())) {
      $fields = [
        'webform' => $webform->id(),
        'uid' => $user->id(),
        'created' => $current_time,
      ];

      WebformTimeoutStorage::add($fields);

      $webform_created = $current_time;
    } else {
      $webform_timeout = $element['#timeout'];
      $current_timeout_webform = WebformTimeoutStorage::load($webform->id(), $user->id());
      $webform_created = $current_timeout_webform->created;

      if ($webform_submission->isNew() && (($webform_created + $webform_timeout) > $current_time)) {
        $element['#expired'] = FALSE;
      }
      else {
        $element['#expired'] = TRUE;
        \Drupal::messenger()->addMessage(t('Your form has been expired.'), 'warning');
      }
    }
    $element['#created'] = $webform_created;
  }
}
