/**
 * @file
 * Search form.
 */
(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.webformTimeout = {
    attach: function (context, settings) {
      $('div[data-timeout]').once('timeout-processed').each(function() {
        var $el = $(this);

        var $webform_button = $el.parents('form').find('.webform-button--submit');
        $webform_button.once('timeout-submit-process').bind('click', function() {
          return true;
        });

        var timeout_stamp = parseInt($el.attr('data-created')) + parseInt($el.attr('data-timeout'));
        var now = Math.floor(Date.now() / 1000);
        var distance = timeout_stamp - now;
        if (distance > 0) {
          var timestamp_interval = setInterval(function () {
            now = Math.floor(Date.now() / 1000);
            distance = timeout_stamp - now;

            if ($el.find('em.days').length) {
              var days = Math.floor(distance / (60 * 60 * 24));
              $el.find('em.days').text(days);
            }
            if ($el.find('em.hours').length) {
              var hours = Math.floor((distance % (60 * 60 * 24)) / (60 * 60));
              $el.find('em.hours').text(hours);
            }
            if ($el.find('em.minutes').length) {
              var minutes = Math.floor((distance % (60 * 60)) / (60));
              $el.find('em.minutes').text(minutes.toString().length === 1 ? '0' + minutes.toString() : minutes);
            }
            if ($el.find('em.seconds').length) {
              var seconds = Math.floor((distance % (60)));
              $el.find('em.seconds').text(seconds.toString().length === 1 ? '0' + seconds.toString() : seconds);
            }

            if (distance < 0) {
              clearInterval(timestamp_interval);
              $el.text('');
              $webform_button.trigger('click');
            }
          }, 1000);
        }
        else {
          $el.text('');
        }
      });


    },
  };

})(jQuery, Drupal);
