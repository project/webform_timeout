<?php

namespace Drupal\webform_timeout\Form;

use Drupal\Core\Form\FormBase;
use Drupal\webform\WebformInterface;
use Drupal\Core\Form\FormStateInterface;


/**
 * Enable or disable timeout for the current webform.
 */
class WebformTimeoutForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_timeout_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, WebformInterface $webform = NULL) {

    $elements = $webform->getElementsDecoded();
    $enabled = isset($elements['webform_timeout']);

    $form['webform_timeout'] = [
      '#type' => 'details',
      '#title' => $this->t('Webform Timeout'),
      '#open' => TRUE,
    ];
    $form['webform_timeout']['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable timeout for this webform'),
      '#default_value' => $enabled,
      '#description' => $this->t('If checked, timeout will be enabled for this webform.'),
    ];
    $form['webform_timeout']['timeout'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Seconds'),
      '#default_value' => isset($elements['webform_timeout']) ? $elements['webform_timeout']['#timeout'] : 300,
    ];

    $form['webform_timeout']['timeout']['#states'] = [
      'visible' => [
        ':input[name="enable"]' => [['checked' => TRUE]],
      ],
    ];


    $form['webform'] = [
      '#type' => 'value',
      '#value' => $webform,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $webform = $form_state->getValue('webform');
    $enable = boolval($form_state->getValue('enable'));
    $timeout = $form_state->getValue('timeout');
    $elements = $webform->getElementsDecoded();

    if ($enable) {
      $element = [
        'webform_timeout' => [
          '#type' => 'webform_timeout',
          '#title' => $this->t('Timeout')->render(),
          '#timeout' => $timeout,
        ],
      ];

      $elements = $element + $elements;

      $webform->setElements($elements);
      $webform->save();
    }
    else {
        $webform->deleteElement('webform_timeout');
        $webform->save();
        \Drupal::messenger()->addMessage(t('Timeout mode has been disabled.'), 'warning');
    }
  }
}
