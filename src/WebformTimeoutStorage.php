<?php

namespace Drupal\webform_timeout;

use Drupal;
use Drupal\Core\Database\StatementInterface;
use Exception;

class WebformTimeoutStorage {


  /**
   * @param $webform
   * @param $uid
   * @return bool
   */
  public static function exists($webform, $uid) {
    $result = Drupal::database()->select('webform_timeout', 'wt')
      ->fields('wt', ['webform'])
      ->condition('webform', $webform, '=')
      ->condition('uid', $uid, '=')
      ->execute()
      ->fetchField();
    return (bool) $result;
  }

  /**
   * @param array $fields
   * @return StatementInterface|int|null
   * @throws Exception
   */
  public static function add(array $fields) {
    return Drupal::database()->insert('webform_timeout')->fields($fields)->execute();
  }

  /**
   * @param $webform
   * @param $uid
   * @return
   */
  public static function load($webform, $uid) {
    $result = \Drupal::database()->select('webform_timeout', 'wt')
      ->fields('wt')
      ->condition('webform', $webform, '=')
      ->condition('uid', $uid, '=')
      ->execute()
      ->fetchObject();
    return $result;
  }

  /**
   * @param $webform
   * @param $uid
   *
   * @return int
   */
  public static function delete($webform, $uid) {
    return \Drupal::database()->delete('webform_timeout')
      ->condition('webform', $webform)
      ->condition('uid', $uid)
      ->execute();
  }
}
