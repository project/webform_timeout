<?php

namespace Drupal\webform_timeout\Element;

use Drupal;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a webform timeout render element.
 *
 * @code
 * $build['awesome'] = [
 *   '#type' => 'webform_timeout',
 *   '#timeout' => 300
 * ];
 * @endcode
 *
 * @see plugin_api
 * @see render_example_theme()
 *
 * @RenderElement("webform_timeout")
 */
class WebformTimeout extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {

    // Returns an array of default properties that will be merged with any
    // properties defined in a render array when using this element type.
    // You can use any standard render array property here, and you can also
    // custom properties that are specific to your new element type.
    return [
      // See render_example_theme() where this new theme hook is declared.
      '#theme' => 'webform_timeout',
      // Define a default #pre_render method. We will use this to handle
      // additional processing for the custom attributes we add below.
      '#pre_render' => [
        [self::class, 'preRenderElement'],
      ],
      // This is a custom property for our element type. We set it to blank by
      // default. The expectation is that a user will add the content that they
      // would like to see inside the marquee tag. This custom property is
      // accounted for in the associated template file.
      '#content' => '',
      '#expired' => FALSE,
      '#timeout_days' => '',
      '#timeout_hours' => '',
      '#timeout_minutes' => '',
      '#timeout_seconds' => '',
      '#attributes' => [
        'data-timeout' => 0,
        'data-created' => 0,
      ],
    ];
  }

  /**
   * Pre-render callback; Process custom attribute options.
   *
   * @param array $element
   *   The renderable array representing the element with '#type' => 'webform_timeout'
   *   property set.
   *
   * @return array
   *   The passed in element with changes made to attributes depending on
   *   context.
   */
  public static function preRenderElement(array $element) {
    $element['#attributes']['data-timeout'] = $element['#timeout'];
    $element['#attributes']['data-created'] = $element['#created'];

    $timeOutDate = $element['#created'] + $element['#timeout'];
    $current_time = Drupal::time()->getCurrentTime();

    $distance = $timeOutDate - $current_time;

    if ($distance > 0) {
      $element['#timeout_days'] = floor($distance / (60 * 60 * 24));
      $element['#timeout_hours'] = floor(($distance % (60 * 60 * 24)) / (60 * 60));
      $element['#timeout_minutes'] = str_pad(floor(($distance % (60 * 60)) / (60)), 2, "0", STR_PAD_LEFT);
      $element['#timeout_seconds'] = str_pad(floor(($distance % (60))), 2, "0", STR_PAD_LEFT);
    } else {
      $element['#expired'] = TRUE;
    }

    return $element;
  }

}
